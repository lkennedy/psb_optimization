import typing as t
from logging import getLogger
import time
import numpy as np
from cernml import coi, japc_utils, gym_utils
from gym.spaces import Box 
from pyjapc import PyJapc
from gym.spaces import Box
from pyjapc import PyJapc
from random import random, randint, uniform
from utils.parameters import apply_boundary

LOG = getLogger(__name__)


class line_density_optimization(coi.SingleOptimizable, coi.Configurable):
    metadata = {
        "render.modes": [],
        "cern.japc": True,
        "cern.machine": coi.Machine.PSB,
        "cern.cancellable": True
    }
    DEVICES: t.ClassVar[t.List[str]] = [
        "BA1.FGVRFH1/Settings#amplitudes",
        "BA1.FGVRFH2/Settings#amplitudes",
    ]
    OBSERVABLES: t.ClassVar[str] = "BTM.BSFHV01/Emittance#emittance"

    LOWER_LIMITS = np.array([5e3, 0.])
    UPPER_LIMITS = np.array([20e3, 0.5])

    SETTINGS_SCALE = gym_utils.Scaler(
        Box(
            low=-MACHINE_LIMITS, 
            high=MACHINE_LIMITS, 
            shape=(len(MACHINE_LIMITS),), 
            dtype=np.double
        ),
        symmetric=False
    )

    def __init__(self, japc: PyJapc, cancellation_token: coi.cancellation.Token) -> None:
        """
        Python class initializer
        """    
        self.japc = japc
        self.token = cancellation_token

        self.optimization_space_size = 4
        self.optimization_space = self.SETTINGS_SCALE.scaled_space 
        self.scale_gain = 0.1e3
        
        LOG.info(f"Optimisation space {self.optimization_space}")

    def get_initial_params(self) -> np.ndarray:
        """
        Reads initial parameters for each device when optimisation GUI is started.
        For the voltages, we read the RF cavity voltages for both the first and
        second harmonic and select the maximum point in the voltage program to
        calculate the maximum voltage sum of h1 + h2 for machine protection
        requirements. This maximum sum of voltages is then used to reconstruct
        the rest of the voltage program such that no voltage at any time step
        exceeds 20kV. The final initial parameter is the ratio h2/h1, and then 
        the voltage sum and ratio are changed during optimization to maintain
        the intended voltage program shape.
        """
        h1 = self.japc.getParam(self.DEVICES[0])[0]
        h2 = self.japc.getParam(self.DEVICES[1])[0]
        self._initial_functions = [h1, h2]
        
        h1_max = h1[1][9]
        h2_max = h2[1][9]
        h2_ratio = h2_max / h1_max 
        v_sum = h1_max + h2_max
        
        initial_actors = [v_sum, ratio]
        self.initial_parameters = np.array(initial_actors).reshape((2,)) 
        
        return np.zeros(self.initial_parameters.shape)

    def compute_single_objective(self, params: np.ndarray) -> float:
        """
        The cost function to be minimized (emittance). Take initial parameters
        and steps between -1 and 1 multiplied by the scaling factor. To prevent
        machine damage, apply_boundary is applied to any settings before being
        sent to the machine in order to clip all settings within a safe 
        boundary.
        """
        LOG.info(f"Optimisation around {self.initial_parameters}")
        
        settings = self.initial_parameters + params * self.scale_gain
        settings = apply_boundary(settings, self.LOWER_LIMITS, self.UPPER_LIMITS)

        voltage = settings[0]
        vh2_ratio = settings[1]
        vh1_settings = voltages * (1 - vh2_ratio)
        vh2_settings = voltages * vh2_ratio

        LOG.info(f"Current Optimisation Factors: {params}")
        LOG.info(f"Setting VH1 parameters to: {vh1_settings}")
        LOG.info(f"Setting VH2 parameters to: {vh2_settings}")
    
        for vh1, vh2 in zip(vh1_settings, vh2_settings):
            if vh1 + vh2 > 22e3:
                LOG.info("Voltage exceeded upper bound of 20kV. Stopping optimization.")
                return
            elif vh1 < 0 or vh2 < 0:
                LOG.info("Voltage exceeded lower bound. Stopping optimization.")
                return
                    
        functions_to_send = self._initial_functions
        functions_to_send[0] = self.h1_voltage(vh1_settings) # VH1 function
        functions_to_send[1] = self.h2_voltage(vh2_settings) # VH2 function
    
        for actor_name, function in zip(self.DEVICES, functions_to_send):
            self.japc.setParam(actor_name, function)  

        emittence = self.japc.getNextParamValue(self.OBSERVABLES)

        return emittence

    def h1_voltage(self, input_voltage: float):
        """ 
        RF cavity voltage reconstruction for the first harmonic. Takes the
        maximum point of the voltage program and builds the rest of the points
        from it.
        
        :param input_voltage: point on the function to reconstruct from
        """

        time_steps = [0., 260., 270., 275., 275.504048, 275.6, 276.6, 280.001641, 
                      282.002279, 284.003006, 340.009404, 390.011197, 500.015704, 
                      520.016496, 540.017101, 570.017402, 770.024163, 810., 820.]
        
        ramp_steps = [0., 0., 0.4849, 0.4849, 0.4849, 0.4849, 0.3182, 0.8616, 
                      0.9176, 0.9488, 0.894, 0.9625, 0.9687, 0.9701, 0.9649, 
                      1., 1., 0.] 
     
        voltage = input_voltage * ramp_steps 
        voltage_func = [[t_steps, voltage]]
        return voltage_func

    def h2_voltage(self, input_voltage: float):
        """ 
        RF cavity voltage reconstruction for the second harmonic. Takes the
        maximum point of the voltage program and builds the rest of the points
        from it.
        
        :param input_voltage: point on the function to reconstruct from
        """

        time_steps = [0., 260., 270., 275., 275.504048, 275.6, 276.6, 280.001641,
                      282.002279, 284.003006, 340.009404, 390.011197, 650., 730.,
                      790., 800., 820.]

        ramp_steps = [0., 0., 0.51107, 0.51107, 0.51107, 0.51107, 0.2555, 0.9081,
                      0.9670, 1., 0.9422, 0.8942, 0.8432, 0.511, 0.3833, 0., 0.]

        voltage = input_voltage * ramp_steps
        voltage_func = [[t_steps, voltage]]
        return voltage_func

    def get_config(self) -> coi.Config:
        """
        Congifurable parameters within GUI 
        """
        config = coi.Config()
        config.add(
            "scale_gain",
            self.scale_gain,
            range=(0.05e3, 1e3), 
            label="Max delta [V]"
        )
        return config
        
    def apply_config(self, values: coi.ConfigValues) -> None:
        """
        Apply the config set in get_config
        """
        self.scale_gain = values.scale_gain

    def render(self, mode='human', **kwargs):
        pass

coi.register("PSB-Optimisation-v0", entry_point=line_density_optimization)
