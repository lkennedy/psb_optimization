import typing as t
from logging import getLogger
import time
import numpy as np
from cernml import coi, japc_utils, gym_utils
from gym.spaces import Box 
from pyjapc import PyJapc
from gym.spaces import Box
from pyjapc import PyJapc
from random import random, randint, uniform
from simenv import Simulation
from utils.parameters import cache_params, get_params, apply_boundary

LOG = getLogger(__name__)


class line_density_optimization(coi.SingleOptimizable, coi.Configurable):
    metadata = {
        "render.modes": [],
        "cern.japc": True,
        "cern.machine": coi.Machine.PSB,
        "cern.cancellable": True
    }

    LOWER_LIMITS = np.array([1e3, 0.0, 100e-9])
    UPPER_LIMITS = np.array([20e3, 0.6, 2000e-9])

    SETTINGS_SCALE = gym_utils.Scaler(
        Box(
            low=-1, 
            high=1, 
            shape=(len(UPPER_LIMITS),), 
            dtype=np.double
        )
    )

    def __init__(self, japc: None, cancellation_token: coi.cancellation.Token) -> None:
        """
        Python class initializer
        """    
        self.japc = japc
        self.token = cancellation_token
        self.optimization_space = self.SETTINGS_SCALE.scaled_space 
        
        initial = [4e3, 1e3, 0.47, 200e-9]
        cache_params(initial)   
        self.vh1 = initial[0]
        self.vh2 = initial[1]
        self.vh2_ratio = initial[2]
        self.bunch_length = initial[3]

        self.voltage_gain = 5e3
        self.ratio_gain = 0.3
        self.bunch_gain = 500e-9
        self.scale_gain = np.array([self.voltage_gain, self.ratio_gain, self.bunch_gain])

    def get_initial_params(self) -> np.ndarray:
        """
        Reads initial parameters when start button is pressed inside GUI    
        """
        
        voltage_sum = self.vh1 + self.vh2
        voltage_ratio = self.vh2_ratio 
        bunch_length = self.bunch_length
        self.initial_parameters = np.array([voltage_sum, voltage_ratio, bunch_length])

        return np.zeros(self.initial_parameters.shape)

    def compute_single_objective(self, params: np.ndarray) -> float:
        """
        The cost function to be minimized. The optimizer takes in and acts on 
        scaled values from get_initial_params() but the machine is sent unscaled values.
        """

        initial_parameters = get_params()
        voltage_sum = initial_parameters[0] + initial_parameters[1]
        self.initial_parameters = np.append(voltage_sum, initial_parameters[2::])

        LOG.info(f"Optimisation around {self.initial_parameters}")
        settings = self.initial_parameters + params * self.scale_gain
        settings = apply_boundary(settings, self.LOWER_LIMITS, self.UPPER_LIMITS)
        
        voltage = settings[0]
        self.vh2_ratio_settings = settings[1]
        self.bunch_length_settings = settings[-1]
        self.vh1_settings = voltage * (1 - self.vh2_ratio_settings)
        self.vh2_settings = voltage * self.vh2_ratio_settings

        new_inits = [
            self.vh1_settings, 
            self.vh2_settings, 
            self.vh2_ratio, 
            self.bunch_length_settings
            ]
        
        cache_params(new_inits)
    
        sim = Simulation(
            vh1=self.vh1_settings, 
            vh2=self.vh2_settings, 
            bunch_length=self.bunch_length_settings)

        cost = sim.transverse_emittance()

        return cost

    def get_config(self) -> coi.Config:
        """
        Congifurable parameters within GUI 
        """
        config = coi.Config()
        config.add(
            "scale_gain",
            self.scale_gain,
            range=(0.05e3, 1e3), 
            label="Max delta [V]"
        )
        return config
        
    def apply_config(self, values: coi.ConfigValues) -> None:
        """
        Apply the config set in get_config
        """
        self.scale_gain = values.scale_gain

    def render(self, mode='human', **kwargs):
        pass

coi.register("PSB-Optimisation-v0", entry_point=line_density_optimization)
