import typing as t
import numpy as np
from IPython import display
import time 
import scipy.constants as cont
import re
import subprocess
import sys
import scipy.optimize as opt
import matplotlib.pyplot as plt
import matplotlib.gridspec as gSpec
from matplotlib import cm

#BLonD
from blond.beam.distributions_multibunch import match_beam_from_distribution
from blond.beam.coasting_beam import generate_coasting_beam
from blond.input_parameters.ring import Ring
from blond.input_parameters.ring_options import RingOptions 
from blond.input_parameters.rf_parameters import RFStation
from blond.trackers.tracker import RingAndRFTracker, FullRingAndRF
from blond.beam.beam import Beam, Proton
from blond.beam.profile import Profile
from blond.beam.profile import CutOptions
import blond.beam.coasting_beam as coastBeam
import blond.beam.distributions as distribution 
import blond.impedances.impedance_sources as impSource
import blond.impedances.impedance as imp
from blond.utils.track_iteration import TrackIteration 
import blond.llrf.offset_frequency as offFreq
import blond.llrf.rf_modulation as rfMod
import blond.plots.plot_beams as plBeam


class Simulation:
    """
    Longitudinal Beam Simulation
    """
    def __init__(self,
        intensity=1e8,
        vh1=8e3, 
        vh2=7e3, 
        energy_spread=1e6,
        spread_type='dE', 
        bunch_length=600e-9, 
        t_start=0.0, 
        t_end=10e-3) -> None:
        
        self.bunch_length = bunch_length
        self.energy_spread = energy_spread
        self.spread_type = spread_type
        self.start_time = t_start 
        self.stop_time = t_end
        self.intensity = intensity
        self.vh1 = vh1
        self.vh2 = vh2
        
        self.energy_half_height = 1.0e6
        self.gamma_transition = 4.07
        self.circumference = 2 * np.pi * 25.0 
        self.momentum_compaction = 1 / self.gamma_transition**2
        self.particle_type = Proton()
        self.inj_momentum = 570806250.7953488
        self.n_macro = 1e5
        self.n_slices = 2**7
        self.track_beam()
        self.n_turns = self.ring.n_turns
         
    def track_beam(self):
        """
        Set up and track beam profile.
        """
        ring_options = RingOptions('linear', t_start=self.start_time, t_end=self.stop_time)
        self.ring = Ring(
            ring_length=self.circumference,
            alpha_0=self.momentum_compaction,
            synchronous_data=([self.start_time, self.stop_time], [self.inj_momentum] * 2),
            Particle=self.particle_type,
            RingOptions=ring_options,
        )

        # Beam description
        self.beam = Beam(self.ring, self.n_macro, self.intensity)

        # Tracker
        rf_params = RFStation(
            Ring=self.ring, 
            harmonic=[1, 2], 
            voltage=[self.vh1, self.vh2], 
            phi_rf_d=[np.pi] * 2, 
            n_rf=2
        )
        longitudinal_tracker = RingAndRFTracker(
            RFStation=rf_params, 
            Beam=self.beam, 
            periodicity=False
        )
        self.full_ring_tracker = FullRingAndRF([longitudinal_tracker])

        # Profile
        cut_options = CutOptions(0, self.ring.t_rev[0], self.n_slices)
        cut_options.cut_right = self.ring.t_rev[0]
        cut_options.set_cuts()
        self.profile = Profile(Beam=self.beam, CutOptions=cut_options)

        # Generate beam
        bunch_start = (self.ring.t_rev[0] - self.bunch_length) / 2
        bunch_end = (self.ring.t_rev[0] + self.bunch_length) / 2

        generate_coasting_beam(
            Beam=self.beam, 
            t_start=bunch_start, 
            t_stop=bunch_end, 
            spread=self.energy_spread, 
            spread_type=self.spread_type, 
            energy_offset=0, 
            distribution='gaussian'
        ) 
        self.profile.track()
        self._map = [self.full_ring_tracker.track, self.profile.track]
        self.track_iteration = TrackIteration(self._map)
    
    def transverse_emittance(self):
        emittance = 1
        for t in self.track_iteration:
            if t % 100 == 0:
                emittance *= np.max(self.profile.n_macroparticles) / 5e3
                
        return emittance

    def line_density(self):
        line_density = []
        for t in self.track_iteration:
            line_density.append(np.max(self.profile.n_macroparticles))

        return line_density

    def line_density_wf(self): 
        waterfall = []
        for t in self.track_iteration:
            waterfall.append(self.profile.n_macroparticles.copy())

        return waterfall
    
    def beam_loss(self):
        beam_loss = []
        for t in self.track_iteration:
            if t == 9900:
                particle_times = self.beam.dt
                slow_particles = [dt for dt in particle_times if dt < 0]
                fast_particles = [dt for dt in particle_times if dt > self.ring.t_rev[t]]
                number_lost = len(np.array(slow_particles + fast_particles))
                beam_loss.append(number_lost)

        return 1/beam_loss[-1]

    def plot(self):
        plt.figure()
        plt.scatter(self.beam.dt, self.beam.dE, s=0.5)
        plt.show()
