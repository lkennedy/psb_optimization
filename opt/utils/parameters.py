import pandas as pd
import os
import numpy as np
import typing as t


def cache_params(params, filename='utils/parameters.csv'):

    dict_ = {
        'VH1': params[0], 
        'VH2': params[1],
        'VH2 Ratio': params[2],
        'Bunch Length': params[3]
        }

    df = pd.DataFrame([dict_])
    df.to_csv(filename)


def get_params(filename='utils/parameters.csv'):
    df = pd.read_csv(filename)
    return df.values[0][1::]


def apply_boundary(settings: np.array, lower_bound: t.List[float], upper_bound: t.List[float]):
    for i in range(len(settings)):
        if settings[i] > upper_bound[i]:
            settings[i] = upper_bound[i]
        elif settings[i] < lower_bound[i]:
            settings[i] = lower_bound[i]

    return settings

