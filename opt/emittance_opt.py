import typing as t
from logging import getLogger
import time
import numpy as np
from cernml import coi, japc_utils, gym_utils
from gym.spaces import Box 
from pyjapc import PyJapc
from gym.spaces import Box
from pyjapc import PyJapc
from random import random, randint, uniform
from utils.parameters import apply_boundary

LOG = getLogger(__name__)


class emittance_optimization(coi.SingleOptimizable, coi.Configurable):
    metadata = {
        "render.modes": [],
        "cern.japc": True,
        "cern.machine": coi.Machine.PSB,
        "cern.cancellable": True
    }
    DEVICES: t.ClassVar[t.List[str]] = [
        "BA3.FGVRFH1/Settings#amplitudes",
        "BA3.FGVRFH2/Settings#amplitudes",
    ]
    OBSERVABLES: t.ClassVar[str] = "BTM.BSFHV01/Emittance#emittance"

    LOWER_LIMITS = np.array([5e3, 0.])
    UPPER_LIMITS = np.array([20e3, 0.6])

    SETTINGS_SCALE = gym_utils.Scaler(
        Box(
            low=LOWER_LIMITS, 
            high=UPPER_LIMITS, 
            shape=(len(UPPER_LIMITS),), 
            dtype=np.double
        ),
        symmetric=False
    )

    def __init__(self, japc: PyJapc, cancellation_token: coi.cancellation.Token) -> None:
        """
        Python class initializer
        """    
        self.japc = japc
        self.token = cancellation_token

        self.optimization_space_size = 4
        self.optimization_space = self.SETTINGS_SCALE.scaled_space 
        self.scale_gain = np.array([5e3, 0.2])
        
        LOG.info(f"Optimisation space {self.optimization_space}")

    def get_initial_params(self) -> np.ndarray:
        """
        Reads initial parameters for each device when optimisation GUI is started.
        For the voltages, we read the RF cavity voltages for both the first and
        second harmonic and select the maximum point in the voltage program to
        calculate the maximum voltage sum of h1 + h2 for machine protection
        requirements. This maximum sum of voltages is then used to reconstruct
        the rest of the voltage program such that no voltage at any time step
        exceeds 20kV. The final initial parameter is the ratio vh2/v_sum, and then 
        the voltage sum and ratio are changed during optimization to maintain
        the intended voltage program shape.
        """
        h1 = self.japc.getParam(self.DEVICES[0])
        h2 = self.japc.getParam(self.DEVICES[1])
        self._initial_functions = [h1, h2]
        
        h1_max = h1[0][1][9]
        h2_max = h2[0][1][9]
        v_sum = h1_max + h2_max
        h2_ratio = h2_max / v_sum
        self._initial_actors = [v_sum, h2_ratio] 
        self.initial_parameters = np.array(self._initial_actors).reshape((2,)) 
        
        return np.zeros(self.initial_parameters.shape)

    def compute_single_objective(self, params: np.ndarray) -> float:
        """
        The cost function to be minimized (emittance). Take initial parameters
        and steps between -1 and 1 multiplied by the scaling factor. To prevent
        machine damage, apply_boundary is applied to any settings before being
        sent to the machine in order to clip all settings within a safe 
        boundary.
        """
        LOG.info(f"Optimisation around {self.initial_parameters}")
         
        settings = self.initial_parameters + params * self.scale_gain
        LOG.info(f"Current VSUM/RATIO: {settings[0]}/{settings[1]}")

        voltage = settings[0]
        vh2_ratio = settings[1]
        vh1_settings = voltage * (1 - vh2_ratio)
        vh2_settings = voltage * vh2_ratio
        self.check_safety(settings, voltage, vh2_ratio)    
         
        LOG.info(f"Current Optimisation Factors: {params}")
        LOG.info(f"Setting VH1 parameters to: {vh1_settings}")
        LOG.info(f"Setting VH2 parameters to: {vh2_settings}")

        functions_to_send = self._initial_functions
        functions_to_send[0] = self.voltage_function(vh1_settings, self._initial_functions[0])
        functions_to_send[1] = self.voltage_function(vh2_settings, self._initial_functions[1]) 
    
        for device, function in zip(self.DEVICES, functions_to_send):
            self.japc.setParam(device, function)  

        emittance = self.japc.getNextParamValue(self.OBSERVABLES)
        return emittance #/ self.transmission

    def check_safety(self, settings, voltage_sum, vh2_ratio) -> bool:
        #injection_intensity = self.japc.getParam(self.OBSERVABLES)[30]
        #extraction_intensity = self.japc.getParam(self.OBSERVABLES)[-30]
        #self.transmission = extraction_intensity / injection_intensity
        #loss_tolerance = 0.95

        # done conditions required for terminating episodes and machine safety
        done = bool(
            voltage_sum > self.UPPER_LIMITS[0]
            or voltage_sum < self.LOWER_LIMITS[0]
            or vh2_ratio > self.UPPER_LIMITS[1]
            or vh2_ratio < self.LOWER_LIMITS[1]
            #or self.transmission < loss_tolerance
            )

        if done:
            settings = apply_boundary(settings, self.LOWER_LIMITS, self.UPPER_LIMITS)
            LOG.info(f"Current Settings Unsafe. Clipping settings to {settings}.")

    def voltage_function(self, input_voltage: float, initial_voltage: np.ndarray):
        """ 
        Voltage function reconstruction

        :param input_voltage: point on the function to reconstruct from
        :param initial_voltage: initial voltage function to preserve function shape 
        """
        peak_id = 9
        inj_ids = range(peak_id)
        steps = np.array([0.9082, 0.9671, 1.0, 0.942, 0.894])

        voltage_func = initial_voltage
        inj_voltage = voltage_func[0][1][inj_ids]

        inj_voltage[2:4] = input_voltage * 0.50
        inj_voltage[4::] = input_voltage * steps

        voltage_func[0][1][inj_ids] = inj_voltage

        return voltage_func

    def get_config(self) -> coi.Config:
        """
        Congifurable parameters within GUI 
        """
        config = coi.Config()
        config.add(
            "scale_gain",
            self.scale_gain,
            range=(0.05e3, 1e3), 
            label="Max delta [V]"
        )
        return config
        
    def apply_config(self, values: coi.ConfigValues) -> None:
        """
        Apply the config set in get_config
        """
        self.scale_gain = values.scale_gain

    def render(self, mode='human', **kwargs):
        pass

coi.register("PSB-Optimisation-v0", entry_point=emittance_optimization)
