import time
import typing as t

import gym
import numpy as np
import matplotlib.pyplot as plt

from gym import spaces
from utils.parameters import apply_boundary
from utils.blondsim import Simulation
from utils.plotting import plot_results 

from stable_baselines3.common.callbacks import BaseCallback
from stable_baselines3.common.logger import Figure


class SimulationEnv(gym.Env):
    """
    Custom OpenAI gym environment for simulation training purposes.
    """
    metadata = {'render.modes': ['human']}
    lower_bounds = np.array([6e3, 0.40])
    upper_bounds = np.array([19e3, 0.60])

    def __init__(self, initial_state: t.List[float], nb_steps_per_episode: int=20,
                    min_steps_per_episode: int=20,  v_step: float=0.04, 
                    ratio_step: float=0.04, epsilon_threshold: float=0.093) -> None:
        """
        :param nb_steps_per_episode: maximum number of steps per episode
        :param min_steps_per_episode: minimum number of steps per episode
        :param v_step: the maximum voltage sum increment to be taken on the state
        :param v_ratio: the maximum vh2 ratio increment to be taken on the state
        :param eps_threshold: the targetted emittance value
        """
        self.episode = 0
        self.current_step = 0
        self.max_steps = nb_steps_per_episode
        self.min_steps = min_steps_per_episode
        self.initial_state = initial_state 
        
        self.step_gain = np.array([v_step, ratio_step])
        self.action_space = spaces.Box(
                low=-1, 
                high=1,
                shape=(len(self.step_gain),)
                )

        self.observation_space = spaces.Box(
                low=self.lower_bounds,
                high=self.upper_bounds,
                shape=(len(self.step_gain),)
                )
        
        self.state = np.array(initial_state)
        self.reward_threshold = 1 / epsilon_threshold # 10.526
        self.episode_threshold = 100.
        self.episode_reward = 0

        # plotting reqs
        self.obs_memory = []
        self.rw_memory = []

    def step(self, action) -> t.Tuple[np.ndarray, float, bool, t.Dict]:
        """
        Executes the next step in the training process, making changes to the
        environment state and receiving observation-based rewards. Any voltage
        settings outside of the boundary are clipped to be within the boundary.

        :param action: the action taken by the agent network on the environment.
        :return updated state, reward for the state-action pair, done check for 
        episode termination and an information dictionary.
        """
        state, reward = self._take_action(action)
        self.rw_memory.append(reward)
        
        # done conditions required for terminating episodes 
        done = bool(
            self.voltage_sum > self.upper_bounds[0]
            or self.voltage_sum < self.lower_bounds[0]
            or self.vh2_ratio > self.upper_bounds[1]
            or self.vh2_ratio < self.lower_bounds[1]
            or self.current_step == self.max_steps
            #or reward > self.reward_threshold
            )
        
        # clip the settings
        if done:
            state = apply_boundary(
                settings=self.state,
                low=self.lower_bounds, 
                high=self.upper_bounds
                )

        # run for minimum number of steps
        if self.current_step < self.min_steps:
            done = False
        
        print(f"ep {self.episode}.{self.current_step}:")
        print(f"state: [{self.vh1:.0f}, {self.vh2:.0f}]")
        print(f"step_rw: {reward:.2f}, ep_rw: {self.episode_reward:.2f}")
    
        return state, reward, done, {}

    def _take_action(self, action: t.List[float]) -> t.List[float]:
        """
        Updates the state space based on the action taken by the agent nework.

        :param action: the action taken by the agent network on the environment.
        :return the updated state and the reward.
        """ 
        self.current_step += 1
        self.state += (self.upper_bounds * action * self.step_gain)
        return self.state, self._get_reward()


    def _get_reward(self) -> float:
        """
        Reward program based on emittance observations. To minimize the emittance
        we maximize the reward given by 1/emittance while only providing positive
        rewards for sufficiently good emittance performance.
        """
        
        self.voltage_sum = self.state[0]
        self.vh2_ratio = self.state[1]
        self.vh1 = self.voltage_sum * (1 - self.vh2_ratio)
        self.vh2 = self.voltage_sum - self.vh1

        sim = Simulation(vh1=self.vh1, vh2=self.vh2)
        emittance = sim.transverse_emittance()
        self.obs_memory.append(emittance)
        reward = 1 / emittance

        if reward > self.reward_threshold:
            reward = 10. * (reward - self.reward_threshold)
        else:
            reward = -10. * (1 - 0.05 * reward)

        self.episode_reward += reward
        return reward 

    def reset(self) -> np.ndarray:
        """
        Reverts current state back to initial state.
        """
        self.state = self.initial_state
        self.current_step = 0       
        self.episode += 1
        self.episode_reward = 0
        self.rw_memory = []
        self.obs_memory = []
        return self.state
    
    def render(self, mode='human'):
        fig = plt.figure()
        ax0 = fig.add_subplot(211)
        ax1 = fig.add_subplot(212)

        ax0.plot(np.arange(self.current_step), self.rw_memory)
        ax0.set(title='reward\n', xlabel='step no.', ylabel='reward')
        ax1.plot(np.arange(self.current_step), self.obs_memory)
        ax1.set(title='\nobservation', xlabel='step no.', ylabel='emittance')
        plt.tight_layout()
        plt.pause(0.1)
        
    def close(self):
        pass

