import time
import typing as t

import gym
import pyjapc
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

from gym import spaces
from scipy.constants import e
from utils.blondsim import Simulation
from utils.plotting import plot_results 
from utils.parameters import apply_boundary, reconstruct_voltage

from stable_baselines3.common.logger import Figure
from stable_baselines3.common.callbacks import BaseCallback


class RFOptimization(gym.Env):
    """
    Custom OpenAI gym environment for RF cavity optimizations.
    """
    metadata = {'render.modes': ['human']}
    japc = pyjapc.PyJapc("PSB.USER.MD1")

    variables = {
                "VH1": "BA2.FGVRFH1/Settings#amplitudes",
                "VH2": "BA2.FGVRFH2/Settings#amplitudes",
                "Phase": "BA2.FGPRFH2/Settings#amplitudes",
                "Bunch_length": None,
                "Energy_spread": None
                }

    observables = {
                   "Emittance": "BTM.BSFHV01/Emittance#emittance",
                   "Bunch_profile": "BR.SCOPE31.CH02/Acquisition#value",
                   "Intensity": "BR2.BCT-ST/Samples#samples",
                   }

    def __init__(self, max_steps_per_episode: int=20, min_steps_per_episode: int=5,
                 emittance_threshold: float=1.3 loss_tolerance: float=0.90,
                 v_step: float=0.10, ratio_step: float=0.05
                 ) -> None:
        """ 
        :param max_steps_per_episode: maximum number of steps per episode
        :param min_steps_per_episode: minimum number of steps per episode
        :param v_step: the maximum voltage sum increment to be taken on the state
        :param v_ratio: the maximum vh2 ratio increment to be taken on the state
        :param emittance_threshold: the targetted emittance value
        """
        self.episode = 0
        self.current_step = 0
        self.absolute_step = 0
        self.ramp_peak_id = 6
        self.ramp_end_id = 9
        self.extraction_id = -27
        self.max_steps = max_steps_per_episode
        self.min_steps = min_steps_per_episode
        self.emittance_threshold = emittance_threshold
        self.loss_tolerance = loss_tolerance 
        self.step_gain = np.array([v_step, ratio_step])
        
        self.lower_bounds = np.array([8e3, 0.25])
        self.upper_bounds = np.array([20e3, 0.55])
        self.action_space = spaces.Box(low=-1, high=1)

        obs_low = np.array([1., 0.])
        obs_high = np.array([2., 1.])
        self.observation_space = spaces.Box(
                low=obs_low,
                high=obs_high,
                dtype=np.float64
                )
        
        self.tsteps = [0., 260., 270., 275.6, 280., 282., 284., 340., 390.]
        data_labels = ["VH1", "VH2", "Emittance", "Intensity", 
                       "Transmission", "Phase Offset", "Reward"]

        self.df = pd.DataFrame(columns=data_labels)
        self._get_initial_params()

    def step(self, action) -> t.Union[np.ndarray, float, bool, t.Dict]:
        """
        Executes the next step in the training process, making changes to the
        environment state and receiving observation-based rewards. Any voltage
        settings outsidr of the boundary are clipped to be within the boundary.

        :param action: the action taken by the agent in [-1, 1].
        :return updated state, reward for the state-action pair, done check for 
        episode termination and an information dictionary.
        """
        self.voltage_settings += (self.upper_bounds * action * self.step_gain)

        # check if safe
        self.clipped = False
        not_safe = self._check_machine_safety()
        if not_safe:
            self._clip_settings()

        self.send_settings()
        self.reward = self._get_reward()
        state = np.array([self.emittance, self.transmission])
        
        self.render()
        return self.state, self.reward, self.done, {}
        
    def _get_reward(self) -> float:
        """
        Reward program based on emittance observations. To minimize the emittance
        we maximize the reward given by 1/emittance while only providing positive
        rewards for sufficiently good emittance performance.
        """
        self.emittance = self.japc.getNextParamValue(self.observables["Emittance"])
        reward = -self.emittance / (self.transmission**4) 
        if self.transmission > self.loss_tolerance and self.emittance < self.emittance_threshold:
            reward = reward / 5.

        if self.clipped == True:
            reward = reward * 5.

        return reward

    def _clip_settings(self) -> None:
        """
        If the action taken places the voltage profiles in an unsafe state, the 
        settings are clipped to be within a safe boundary and the ep is terminated.
        """
        for i in range(len(self.voltage_settings)):
            if self.voltage_settings[i] > self.upper_bounds[i]:
                self.voltage_settings[i] = self.upper_bounds[i]
            elif self.voltage_settings[i] < self.lower_bounds[i]:
                self.voltage_settings[i] = self.lower_bounds[i]

        print('Episode terminated due to illegal step.')
        self.clipped = True
        self.voltage_sum = self.voltage_settings[0]
        self.vh2_ratio = self.voltage_settings[1]

    def _check_machine_safety(self) -> bool:
        """
        Checks if the beam played and if transmission falls below the loss_tolerance.
        If True, or if any of the voltage conditions fall outside of the boundary,
        the episode is terminated and the settings are clipped to protect.
        """
        while True:
            intensity = self.japc.getNextParamValue(self.observables["Intensity"])
            if np.max(intensity) > 10:
                break

        self.transmission = intensity[self.extraction_id] / np.max(intensity) 
        self.done = bool(self.current_step == self.max_steps)

        unsafe = bool(
            self.voltage_sum > self.upper_bounds[0]
            or self.voltage_sum < self.lower_bounds[0]
            or self.vh2_ratio > self.upper_bounds[1]
            or self.vh2_ratio < self.lower_bounds[1]
            or self.transmission < self.loss_tolerance
            )

        if unsafe:
            self.done = True

        self.voltage_sum = self.voltage_settings[0]
        self.vh2_ratio = self.voltage_settings[1]
        return self.done

    def reconstruct_voltage(self, input_voltage: float, initial_voltage: np.ndarray
                            ) -> np.ndarray:
        """ 
        Voltage function reconstruction for an LHC-type beam.
        :param input_voltage: voltage at the ramp peak to reconstruct downward
        :param init_voltage: init voltage function to preserve function shape 
        """
        ramp_steps = np.array([0.9082, 0.9671, 1.0, 0.942, 0.894])
        initial_voltage[0][1][2:4] = input_voltage * 0.50
        initial_voltage[0][1][4:self.ramp_end_id] = input_voltage * ramp_steps
        return initial_voltage 

    def phase_correction(self, phase_initial: np.ndarray, B_dot_init: np.ndarray, 
                         vh1_init: np.ndarray, vh2_new: np.ndarray) -> np.ndarray:
        """
        Corrects the phase offset in VH2.
        :param phase_init: reference phase 
        :param B_dot_init: reference B_dot
        :param vh1_init: reference VH1 program
        :param vh2_new: the updated current VH2 program
        """
        e = 1.
        R = 25.
        rho = 8.239
        k = 1e-4 / 1e-3
        
        phase = phase_initial.copy()
        v_ref = vh1_init[0][1][0:self.ramp_end_id]
        vh2_new = vh2_new[0][1][0:self.ramp_end_id]
        B_dot_ids = [np.where(B_dot_init[0][0] == step)[0][0] for step in self.tsteps]
        B_dot = B_dot_init[0][1][B_dot_ids] * k
        
        E_gain = 2 * np.pi * R * e * rho * B_dot 
        phi_offset = np.arcsin(E_gain / v_ref) - np.arcsin(E_gain / vh2_new)
        phase[0][1][0:self.ramp_end_id] += phi_offset * 180 / np.pi
        return phase 

    def send_settings(self) -> None:
        """
        Calculates the updated VH1, VH2 and Phase values based on the
        V_sum and ratio return by the agent action and sends them to the
        machine
        """
        # reconstruct voltage and correct the phase offset
        self.new_inj_vh1 = self.voltage_sum * (1 - self.vh2_ratio)
        self.new_inj_vh2 = self.voltage_sum - self.new_inj_vh1
        vh1 = self.reconstruct_voltage(self.new_inj_vh1, self.vh1_init)
        vh2 = self.reconstruct_voltage(self.new_inj_vh2, self.vh2_init)
        self.phase = self.phase_correction(self.phase_init, self.B_dot_init, self.vh1_init, vh2)

        self.japc.setParam(self.variables['VH1'], vh1)
        self.japc.setParam(self.variables['VH2'], vh2)
        self.japc.setParam(self.variables['Phase'], self.phase)

    def _get_initial_params(self) -> None: 
        """
        Gets the initial observation state and machine settings before converting
        machine settings to our actionable parameters, i.e converting the 
        VH1 and VH2 settings to volage_sum and vh2_ratio across the ramp ids.
        """
        # machine settings
        self.B_dot_init = np.array(self.japc.getParam("PSBBEAM/BDot"))
        self.phase_init = np.array(self.japc.getParam(self.variables['Phase']))
        self.vh1_init = np.array(self.japc.getParam(self.variables['VH1']))
        self.vh2_init = np.array(self.japc.getParam(self.variables['VH2']))

        # observations
        emittance = self.japc.getParam(self.observables["Emittance"])
        intensity = self.japc.getParam(self.observables["Intensity"])
        transmission = intensity[self.extraction_id] / np.max(intensity)
        self.initial_state = np.array([emittance, transmission])
        
        # injection window voltages
        vh1_peak_init = self.vh1_init[0][1][self.ramp_peak_id]
        vh2_peak_init = self.vh2_init[0][1][self.ramp_peak_id]

        # action params V_sum and ratio
        voltage_sum_init = vh1_peak_init + vh2_peak_init
        vh2_ratio_init = vh2_peak_init / voltage_sum_init
        self.settings = np.array([voltage_sum_init, vh2_ratio_init])
        self.voltage_settings = self.settings.copy()
        self.initiate_metrics()

    def save_data(self) -> None:
        """
        Save training metrics and results 
        """
        self.reward_memory.append(self.reward)
        self.episode_reward.append(self.reward)
        self.observation_memory.append(self.emittance)
        self.transmission_memory.append(self.transmission)
        self.vh1_memory.append(self.new_inj_vh1)
        self.vh2_memory.append(self.new_inj_vh2)
        self.vsum_memory.append(self.voltage_sum / 1000)
        self.vratio_memory.append(self.vh2_ratio)
        tf.summary.scalar('emittance', data=self.emittance)         
        tf.summary.scalar('transmission', data=self.transmission)         
        
        updated_df = pd.DataFrame(
                        {
                        "VH1": [self.new_inj_vh1], 
                        "VH2": [self.new_inj_vh2],
                        "Emittance": [self.emittance], 
                        "Intensity": [self.intensity], 
                        "Transmission": [self.transmission], 
                        "Phase Offset": [self.phase],
                        "Reward": [self.reward]
                        }
                      )

        self.df = self.df.append(updated_df)
        self.df.to_csv("training_dataframe.csv")

    def render(self, mode='human') -> None:
        """
        Method for plotting training data metrics.
        """
        self.current_step += 1
        self.absolute_step += 1

        self.save_data()
        self.logging()
        plt.clf()

        fig, ((ax0, ax1), (ax2, ax3)) = plt.subplots(2, 2, figsize=(10, 8))
        ax0.scatter(np.array(self.vsum_memory), np.array(self.vratio_memory), 
                        s=2, c=self.emittance)
        ax0.scatter(np.array(self.vsum_memory)[-1], np.array(self.vratio_memory)[-1], color='r')
        ax0.set(xlabel=r'$V_{sum}$ [kV]', ylabel=r'$V_{ratio}$', title='Actions')
        ax1.plot(np.arange(self.absolute_step), self.observation_memory)
        ax1.set(xlabel='Step', title=r'Emittance')
        ax2.plot(np.arange(self.absolute_step), 100 * np.array(self.transmission_memory))
        ax2.set(xlabel='Step', title=r'Transmission %')
        ax3.contourf(self.profile, levels=50)
        ax3.set(xlabel='Time [ns]', title='Longitudinal Profile')
        ax0.set_xlim((self.lower_bounds[0], self.upper_bounds[0]))
        ax0.set_ylim((self.lower_bounds[1], self.upper_bounds[1]))
        plt.tight_layout()

    def logging(self) -> None:
        print(f"Episode: {self.episode}.{self.current_step}:")
        print(f'Emittance: {self.emittance:.3f}')
        print(f'Transmission: {self.transmission:.3f}')
        print(f"VH1: {self.new_inj_vh1:.0f}, VH2: {self.new_inj_vh2:.0f}")
        print(f"Vsum: {self.voltage_sum:.0f}, Vratio: {self.vh2_ratio}")
        print(f"Reward: {self.reward:.2f}")
        print(f"--------------------------------------")

    def initiate_metrics(self) -> None:
        """
        Training plots metric placeholders
        """
        self.reward_memory = []
        self.episode_reward = []
        self.episode_mean = []
        self.observation_memory = []
        self.transmission_memory = []
        self.vh1_memory = []
        self.vh2_memory = []
        self.vsum_memory = []
        self.vratio_memory = []

    def reset(self) -> np.ndarray:
        """
        Reverts current state back to init state when the episode ends.
        """
        self.voltage_settings = self.settings
        self.state = self.initial_state
        self.current_step = 0       
        self.episode += 1
        self.episode_mean.append(np.mean(self.episode_reward))
        self.episode_reward = []
        self.vh1_memory = []
        self.vh2_memory = []
        return self.state
