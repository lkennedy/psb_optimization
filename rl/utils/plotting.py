import numpy as np
import matplotlib.pyplot as plt

from mpl_toolkits.axes_grid1 import make_axes_locatable
from utils.parameters import load_object
from stable_baselines3.common.results_plotter import load_results, ts2xy


def plot_results(log_folder, title='Learning'):
    """ 
    plot training results for RL agent

    :param log_folder: (str) the save location of the results to plot
    :param title: (str) the title of the task to plot
    """
    x, y = ts2xy(load_results(log_folder), 'episodes')
    x0, y0 = ts2xy(load_results(log_folder), 'timesteps')

    fig, ax1 = plt.subplots()
    ax1.plot(x0, y0)
    ax1.set(xlabel='Step', ylabel='Reward')
    plt.savefig('plots/results.jpg')
    plt.show()


def plot2d(input_name='scanner.pkl', output_name='plots/scanner.jpg', mode=None):
    obj = load_object(input_name)
    X = obj[0]
    Y = obj[1]
    Z = obj[-1]

    if mode == 'log':
        Z = np.log(Z)
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set(xlabel='VH1', ylabel='VH2', title='Emittance Scan')

    extent = np.min(X), np.max(X), np.min(Y), np.max(Y)
 
    im = ax.imshow(Z, cmap='viridis_r', vmin=np.min(Z), vmax=np.max(Z), extent=extent, aspect='auto')

    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    fig.colorbar(im, cax=cax)
    plt.draw
    plt.savefig(output_name, dpi=200)
    plt.show()
 
