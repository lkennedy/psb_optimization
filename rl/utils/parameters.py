import os
import typing as t

import pickle
import pandas as pd
import numpy as np
import time

#from utils.blondsim import Simulation


def cache_params(params, filename='utils/parameters.csv'):
    """
    For GeOFF simulation purposes, stores initial parameters from latest 
    update. Necessary since no actual settings are sent to the machine, 
    therefore get_initial_parameters() would never be update. 

    :param params: list of parameter values to be cached (dummy sent to machine)
    """

    dict_ = {
        'VH1': params[0], 
        'VH2': params[1],
        'VH2 Ratio': params[2],
        'Bunch Length': params[3]
        }

    df = pd.DataFrame([dict_])
    df.to_csv(filename)


def get_params(filename='utils/parameters.csv'):
    """
    For GeOFF simulation purposes, gets the stored initial parameters from latest 
    update. Necessary since no actual settings are sent to the machine, 
    therefore initial_parameters would never be updated. 

    :param filename: filename to acquire data 
    """

    df = pd.read_csv(filename)
    return df.values[0][1::]


def apply_boundary(settings: np.array, low: t.List[float], high: t.List[float]):
    """
    Clips the settings sent to machine to be within boundary

    :param settings: machine settings to be clipped
    :param low: lower boundary values for input settings
    :param high: upper boundary values for input settings
    """

    for i in range(len(settings)):
        if settings[i] > high[i]:
            settings[i] = high[i]
        elif settings[i] < low[i]:
            settings[i] = low[i]
    return settings


def parametric_scan(x_range: t.List[float], y_range: t.List[float], z_range: t.List[float] = None, n_grid: int = 100, filename='scanner.pkl'):
    """ 
    Parametric scanner for simulation purposes. Scans a 2 dimensional space
    if z_range is None otherwise scans 3 dimensional space.

    :param x_range: list containing scanning range for x param - [low, high]
    :param y_range: list containing scanning range for y param - [low, high]
    :param z_range: list containing scanning range for z param - [low, high]
    :param n_grid: number of grid points to scan
    """

    x = np.linspace(x_range[0], x_range[-1], n_grid)
    y = np.linspace(y_range[0], y_range[-1], n_grid)

    if z_range == None:
        z = np.zeros(x.shape)
        scan = np.zeros((n_grid, n_grid))

        for i, x_value in enumerate(x):
            for j, y_value in enumerate(y):
                sim = Simulation(vh1=x_value, vh2=y_value)
                scan[i][j] = sim.transverse_emittance() 
    else:
        z = np.linspace(z_range[0], z_range[-1], n_grid)
        scan = np.zeros((n_grid, n_grid, n_grid))

        for i, x_value in enumerate(x):
            for j, y_value in enumerate(y):
                for k, z_value in enumerate(z):
                    sim = Simulation(vh1=x_value, vh2=y_value, bunch_length=z_value)
                    scan[i][j][k] = sim.transverse_emittance()

    df = [x, y, z, scan]
    save_object(df, name=filename)

    return scan


def h1_voltage(input_voltage: float):
    """ 
    RF cavity voltage reconstruction for the first harmonic. Takes the
    maximum point of the voltage program and builds the rest of the points
    from it.
    
    :param input_voltage: point on the function to reconstruct from
    """

    time_steps = [0., 260., 270., 275., 275.504048, 275.6, 276.6, 280.001641, 
                  282.002279, 284.003006, 340.009404, 390.011197, 500.015704, 
                  520.016496, 540.017101, 570.017402, 770.024163, 810., 820.]
    
    #ramp_steps = [0., 0., 0.4849, 0.4849, 0.4849, 0.4849, 0.3182, 0.8616, 
    #              0.9176, 1., 0.894, 1., 1., 1., 1., 1., 1., 0.]
 
    ramp_steps = [0., 0., 0.4849, 0.4849, 0.4849, 0.4849, 0.3182, 0.8616, 
                  0.9176, 1., 0.894, 1.0144, 1.021, 1.022, 1.017, 1.054, 1.054, 
                  0.]

    voltage = input_voltage * ramp_steps 
    voltage_func = [[t_steps, voltage]]
    return voltage_func


def h2_voltage(input_voltage: float):
    """ 
    RF cavity voltage reconstruction for the second harmonic. Takes the
    maximum point of the voltage program and builds the rest of the points
    from it.
    
    :param input_voltage: point on the function to reconstruct from
    """

    time_steps = [0., 260., 270., 275., 275.504048, 275.6, 276.6, 280.001641, 
                  282.002279, 284.003006, 340.009404, 390.011197, 650., 730.,
                  790., 800., 820.]
    
    ramp_steps = [0., 0., 0.51107, 0.51107, 0.51107, 0.51107, 0.2555, 0.9081, 
                  0.9670, 1., 0.9422, 0.8942, 0.8432, 0.511, 0.3833, 0., 0.]
   
    voltage = input_voltage * ramp_steps 
    voltage_func = [[t_steps, voltage]]
    return voltage_func


def voltage_function(input_voltage: float, initial_voltage: np.ndarray):
    """ 
    Voltage function reconstruction

    :param input_voltage: point on the function to reconstruct from
    :param initial_voltage: initial voltage function to preserve function shape 
    """

    peak_id = 9
    inj_ids = range(peak_id)
    steps = np.array([0.9082, 0.9671, 1.0, 0.942, 0.894])

    voltage_func = initial_voltage
    inj_voltage = voltage_func[0][1][inj_ids]

    inj_voltage[2:4] = input_voltage * 0.50
    inj_voltage[4::] = input_voltage * steps

    voltage_func[0][1][inj_ids] = inj_voltage

    return voltage_func



# voltage func --- can change the flat part to be it's own action in the range 0.35-0.55 of ramp peak? then the ramp points as function of the step values with action in 0.8-1.0?

def save_object(obj, name):
    with open('data/' + name, 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


def load_object(name):
    with open('data/' + name, 'rb') as f:
        return pickle.load(f)
