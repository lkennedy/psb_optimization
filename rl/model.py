import os

import gym
import numpy as np
import matplotlib.pyplot as plt

from envs.simenv import SimulationEnv
from utils.plotting import plot_results

from stable_baselines3 import TD3, SAC, DDPG
from stable_baselines3.ppo.policies import MlpPolicy
from stable_baselines3.common.evaluation import evaluate_policy
from stable_baselines3.common import results_plotter
from stable_baselines3.common.monitor import Monitor
from stable_baselines3.common.results_plotter import load_results, ts2xy
from stable_baselines3.common.noise import NormalActionNoise
from stable_baselines3.common.callbacks import BaseCallback
from stable_baselines3.common.logger import Figure 
from stable_baselines3.common.env_checker import check_env
from stable_baselines3.common.noise import NormalActionNoise, OrnsteinUhlenbeckActionNoise 

# create env
log = 'weights/'
nb_steps_per_episode = 25
env = SimulationEnv(nb_steps_per_episode=nb_steps_per_episode)
env = Monitor(env, log)  
n_actions = env.action_space.shape[-1]

#check_env(env)
#print(n_actions)


# model
training_steps = 2e4
action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.5 * np.ones(n_actions))
model = SAC(
        'MlpPolicy', 
        env, 
        gamma=0.,
        verbose=1, 
        ent_coef=1.,
        learning_rate=1e-4, 
        learning_starts=100,
        batch_size=128,
        action_noise=action_noise,
        #tensorboard_log='SAC'
        )



model.learn(total_timesteps=training_steps, log_interval=10)
model.save('sac_psb')
