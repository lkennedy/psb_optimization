from utils.parameters import parametric_scan, save_object
from utils.plotting import plot2d


x_range = [6e3, 12e3]
y_range = [2e3, 8e3]
n_grid = 100

parametric_scan(x_range, y_range, n_grid=n_grid)

