import os

import gym
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf

from envs.rf_optimization import RFOptimization
from utils.plotting import plot_results

from stable_baselines3 import TD3, SAC, DDPG
from stable_baselines3.ppo.policies import MlpPolicy
from stable_baselines3.common.evaluation import evaluate_policy
from stable_baselines3.common import results_plotter
from stable_baselines3.common.monitor import Monitor
from stable_baselines3.common.results_plotter import load_results, ts2xy
from stable_baselines3.common.noise import NormalActionNoise
from stable_baselines3.common.callbacks import BaseCallback
from stable_baselines3.common.logger import Figure 
from stable_baselines3.common.env_checker import check_env
from stable_baselines3.common.noise import NormalActionNoise, OrnsteinUhlenbeckActionNoise 

def linear_schedule(initial_value: float):
    def func(progress_remaining: float):
        return progress_remaining * initial_value
    tf.summary.scalar('learning rate', data=func)
    return func

# create env
log = './model_logs/'
nb_steps_per_episode = 25
env = RFOptimization(max_steps_per_episode=nb_steps_per_episode)
env = Monitor(env, log)  
n_actions = env.action_space.shape[-1]
tb_callback = tf.keras.callbacks.Tensorboard()

#check_env(env)
#print(n_actions)

# model
training_steps = 1e4
action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.5 * np.ones(n_actions))
model = SAC(
        'MlpPolicy', 
        env, 
        gamma=0.99,
        verbose=1, 
        ent_coef='auto',
        learning_starts=1000,
        batch_size=2*256,
        action_noise=action_noise,
        tensorboard_log=log,
        learning_rate=linear_schedule(3e-4)
        )


model.learn(total_timesteps=training_steps, tb_log_name='sac', log_interval=10, callback=tb_callback)
model.save('sac_psb')


