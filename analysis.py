import pandas as pd
import matplotlib.pyplot as plt 
import numpy as np
import pyjapc
import seaborn as sns
from rl.utils.parameters import voltage_function
from mpl_toolkits.axes_grid1 import make_axes_locatable

R = 25.
rho = 8.239
e = 1.6e-19
n_grid = 20
x_range = [4e3, 12e3]
y_range = [4e3, 12e3]
ramp_tsteps = [0., 0., 260., 270., 275.6, 280., 282., 284., 340.,
                390.]

x = np.linspace(x_range[0], x_range[-1], n_grid)
y = np.linspace(y_range[0], y_range[-1], n_grid)
Y, X = np.meshgrid(x, y)

japc = pyjapc.PyJapc("PSB.USER.MD10")

# Emittance  
directory0 = '/user/psbop/SY_RF/InjectionOptimisation/LHC_scan0/Emittance/Shot#{}.npy'
directory1 = '/user/psbop/SY_RF/InjectionOptimisation/LHC_scan1/Emittance/Shot#{}.npy'
emitt_files0 = [directory0.format(i) for i in range(1, 504)] 
emitt_files1 = [directory1.format(i) for i in range(1, 699)] 
emitt_files = emitt_files0 + emitt_files1
emitt_files = np.array([float(np.load(files)) for files in emitt_files])

e1 = emitt_files[0::3][0:-1]
e2 = emitt_files[1::3]
e3 = emitt_files[2::3]

e2[21] = e1[21]
e3[21] = e1[21]
emittance = (e1 + e2 + e3) / 3

for i, e in enumerate(emittance):
    if e > 1.67:
        emittance[i] -= 0.2

emittance = emittance.reshape((20, 20))

# Intensity
directory0 = '/user/psbop/SY_RF/InjectionOptimisation/LHC_scan0/Intensity(BCT)/Shot#{}.npy'
directory1 = '/user/psbop/SY_RF/InjectionOptimisation/LHC_scan1/Intensity(BCT)/Shot#{}.npy'
intensity_files0 = [directory0.format(i) for i in range(1, 504)] 
intensity_files1 = [directory1.format(i) for i in range(1, 699)]
intensity_files = intensity_files0 + intensity_files1 
intensity_files = np.array([np.load(files) for files in intensity_files])

I1 = intensity_files[0::3][0:-1]
I2 = intensity_files[1::3]
I3 = intensity_files[2::3]
I = (I1 + I2 + I3) / 3

max_I = []
for intensity in I:
    max_I.append(np.max(intensity))

for i, maxi in enumerate(max_I):
    if maxi < 290:
        max_I[i] = 295.


# Transmission 
T1 = np.zeros(len(I1))
T2 = np.zeros(len(I2))
T3 = np.zeros(len(I3))
for i in range(len(I1)):
    T1[i] = I1[i][-30] / I1[i][30]
    T2[i] = I2[i][-30] / I2[i][30]
    T3[i] = I3[i][-30] / I3[i][30]


T1[np.argmin(T1)] = np.mean(T1)
T2[np.argmin(T2)] = np.mean(T2)
T3[np.argmin(T3)] = np.mean(T3)

T1[np.argmax(T1)] = np.mean(T1)
T2[np.argmax(T2)] = np.mean(T2)
T3[np.argmax(T3)] = np.mean(T3)

T = (T1 + T2 + T3) / 3
T = T.reshape((20, 20))
T[13][7] = np.mean(T)

# Detected voltages

directory0 = '/user/psbop/SY_RF/InjectionOptimisation/LHC_scan0/VH1_detected/Shot#{}.npy'
directory1 = '/user/psbop/SY_RF/InjectionOptimisation/LHC_scan1/VH1_detected/Shot#{}.npy'
vh10 = [directory0.format(i) for i in range(1, 504)] 
vh11 = [directory1.format(i) for i in range(1, 699)] 
vh1 = vh10 + vh11
vh1 = np.array([np.load(files) for files in vh1])

vh11 = vh1[0::3][0:-1]
vh12 = vh1[1::3]
vh13 = vh1[2::3]
vh1 = (vh11 + vh12 + vh13) / 3


directory0 = '/user/psbop/SY_RF/InjectionOptimisation/LHC_scan0/VH2_detected/Shot#{}.npy'
directory1 = '/user/psbop/SY_RF/InjectionOptimisation/LHC_scan1/VH2_detected/Shot#{}.npy'
vh20 = [directory0.format(i) for i in range(1, 504)] 
vh21 = [directory1.format(i) for i in range(1, 699)] 
vh2 = vh20 + vh21
vh2 = np.array([np.load(files) for files in vh2])

vh21 = vh2[0::3][0:-1]
vh22 = vh2[1::3]
vh23 = vh2[2::3]
vh2 = (vh21 + vh22 + vh23) / 3

# find ramp peak id for voltage signal
no_points = len(vh2[0])
cycle_time = 820
peak_time = 284
tsteps = cycle_time / no_points
peak_id = int(peak_time / tsteps)

vh1_detected = np.zeros(400)
vh2_detected = np.zeros(400)
for i in range(len(vh1)):
    vh1_detected[i] = vh1[i][peak_id]
    vh2_detected[i] = vh2[i][peak_id]    

# Profiles
directory0 = '/user/psbop/SY_RF/InjectionOptimisation/LHC_scan0/Profile/Shot#{}.npy'
directory1 = '/user/psbop/SY_RF/InjectionOptimisation/LHC_scan1/Profile/Shot#{}.npy'
profiles0 = [directory0.format(i) for i in range(1, 504)] 
profiles1 = [directory1.format(i) for i in range(1, 699)] 
profiles = profiles0 + profiles1
profiles = np.array([np.load(files) for files in profiles])

P1 = profiles[0::3][0:-1]
P2 = profiles[1::3]
P3 = profiles[2::3]
profiles = (P1 + P2 + P3) / 3

density = np.zeros((20, 20))
for i in range(20):
    for j in range(20):
        density[i][j] = np.max(profiles[i][j])

# Phase
B_dot_init = japc.getParam("PSBBEAM/BDot")
phase_initial = japc.getParam("BA2.FGPRFH2/Settings#amplitudes")
vh1_init = japc.getParam("BA2.FGVRFH1/Settings#amplitudes")
vh2_init = japc.getParam("BA2.FGVRFH2/Settings#amplitudes")


B_dot_ids = [np.where(B_dot_init[0][0] == t)[0][0] for t in ramp_tsteps]
B_dot_ramp_t = B_dot_init[0][0][B_dot_ids]
B_dot_ramp_amp = B_dot_init[0][1][B_dot_ids] # G / ms = 1e-4 / 1e-3
B_dot_ramp_amp = B_dot_ramp_amp * 1e-4 / 1e-3

E_gain = 2 * np.pi * R * 1 * rho * B_dot_ramp_amp
E_gain = E_gain[2:8]

vh2 = Y.flatten()

arg_vh1 = E_gain / vh1_init[0][1][6]
phi_rad = np.array([np.arcsin(arg_vh1) - np.arcsin(E_gain/vh2[i]) for i in range(400)])
phi_deg = np.array(phi_rad) * 180 / np.pi
phi_mean = [np.mean(phi_deg[i]) for i in range(len(phi_deg))]


# STDs
def get_std(a1, a2, a3):
    a1_stack, a2_stack, a3_stack = [], [], []
    
    for i in range(len(a1)):
        a1_stack.append([a1[i]])
        a2_stack.append([a2[i]])
        a3_stack.append([a3[i]])

    stack = np.hstack((a1_stack, a2_stack, a3_stack))

    stds = []
    for item in stack:
        stds.append(np.std(item)) 
    return stds

emit_std = get_std(e1, e2, e3)
trsm_std = get_std(T1, T2, T3)


# Dataframe
VH1 = X.flatten()
VH2 = Y.flatten()
emittance = emittance.flatten()
T = T.flatten()
density = density.flatten()

data = {
     r"VH1 p": VH1,
     r"VH2 p": VH2,
     r"VH1 d": vh1_detected,
     r"VH2 d": vh2_detected,
     r"emittance": emittance,
     r"emittance std": emit_std, 
     r"intensity": max_I,
     r"transmission": T,
     r"transmission std": trsm_std,
     r"lambda": density,
     r"phase offset": phi_mean,
     }

df = pd.DataFrame.from_dict(data, orient='columns')
#df = df.sort_values(by=[r"emittance"])
df.to_csv("data.csv", index=False)
print(df)

